#!/usr/bin/env python
# coding: utf-8

# In[1]:


path = '/home/boturon/proj/submit/'
tab = "/"


# In[4]:


folders = ["QP_CME_1"]
type_up = "student" # "student" or "professor"


# # https://towardsdatascience.com/controlling-the-web-with-python-6fceb22c5f08

# In[2]:


import os
import gc
import datetime 
from time import sleep
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.support.ui import WebDriverWait


# In[3]:


options = Options()
options.log.level = "trace"
options.set_headless(headless=True)


# In[5]:


todo = [file          for school in folders          for file in list(reversed(sorted(os.listdir(path+"files/"+school))))][:1]#[0::2]


# In[6]:


if :
    while len(todo)>0:
        try:
            driver = webdriver.Firefox(options=options)
            driver.get('https://kceconedu.surveycto.com/index.html')
            assert driver.current_url=='https://kceconedu.surveycto.com/index.html'
            with open('/home/boturon/access') as f: access = f.read().splitlines()
            
            sleep(10)
            driver.find_element_by_id("login-username").send_keys(access[0])
            sleep(10)
            driver.find_element_by_id("submitBtn").click()
            sleep(10)
            driver.find_element_by_id("login-password").send_keys(access[1])
            sleep(10)
            driver.find_element_by_id("submitBtn").click()       
            
            todo_cp = todo.copy()
            for file in todo_cp:
                print('begin',file,datetime.datetime.now())
                driver.refresh()
                sleep(60)
                assert driver.current_url == 'https://kceconedu.surveycto.com/main.html'
                driver.find_element_by_xpath("//div[7]/div/div[2]/div/div/button/i").click()
                sleep(5)
                driver.find_element_by_link_text("Upload form definition").click()
                driver.find_element_by_name("form_def_file").send_keys(path+'files/'+"_".join(file.split("_")[:3])+"/"+file)
                for image in os.listdir("files/images_books/"):
                    driver.find_element_by_name("datafile").send_keys(path+'files/images_books/'+image)
                driver.find_element_by_xpath("//button[contains(.,' Upload')]").click()
                print('end',file,datetime.datetime.now())
                todo.pop(0)
                sleep(5)
        except:
            print("Trying again")
            sleep(5)
            del driver
            gc.collect()
elif type_up=="professor":
    while len(todo)>0:
        try:
            driver = webdriver.Firefox(options=options)
            driver.get('https://kceconedu.surveycto.com/index.html')
            assert driver.current_url=='https://kceconedu.surveycto.com/index.html'
            with open('/home/boturon/access') as f: access = f.read().splitlines()
            
            sleep(10)
            driver.find_element_by_id("login-username").send_keys(access[0])
            sleep(10)
            driver.find_element_by_id("submitBtn").click()
            sleep(10)
            driver.find_element_by_id("login-password").send_keys(access[1])
            sleep(10)
            driver.find_element_by_id("submitBtn").click()       
            
            todo_cp = todo.copy()
            for file in todo_cp:
                print('begin',file,datetime.datetime.now())
                driver.refresh()
                sleep(60)
                assert driver.current_url == 'https://kceconedu.surveycto.com/main.html'
                driver.find_element_by_xpath("//div[7]/div/div[2]/div/div/button/i").click()
                sleep(5)
                driver.find_element_by_link_text("Upload form definition").click()
                driver.find_element_by_name("form_def_file").send_keys(path+'files/'+"_".join(file.split("_")[:3])+"/"+file)
                driver.find_element_by_xpath("//button[contains(.,' Upload')]").click()
                
                driver.find_element_by_id("id=id-tab-collect").click()
                driver.refresh()
                sleep(60)
                driver.find_element_by_xpath("xpath=//div[4]/div[3]/div/div/div[2]/div/div[2]/a[4]/button/i")
                driver.find_element_by_xpath("xpath=//label[contains(.,' Allow anonymous form access')]")
                driver.find_element_by_xpath("xpath=//button[contains(.,' Save')]")
                print('end',file,datetime.datetime.now())
                todo.pop(0)
                sleep(5)
        except:
            print("Trying again")
            sleep(5)
            del driver
            gc.collect()

